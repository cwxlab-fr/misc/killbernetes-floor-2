# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/helm" {
  version = "2.13.0"
  hashes = [
    "h1:hQIozFWLJk67bKslnNIZPHYb037+6uO86sHVkntPjXY=",
    "zh:016e42bea1c9145b0856bfcf1e5faf657e40e9a94e4d80bee9e0b8742eb9f5fd",
    "zh:0a325cfcb62d4c611a9a7854d2ca26ee8cbd27a1cae40f607c0966e36a858358",
    "zh:2e22929aa1cc59c02e1cb8af8cee25063a706cdfc15d3aff242c8bf76cd12ea3",
    "zh:35d989aa6f43d6401077c190c3262c6df434290c5bec978079ae69eb33f3929e",
    "zh:4cc42ee66af3fa965424c19904e5ac52326d4a31df066d565d591d0e46e64c2d",
    "zh:69a429be3f7183f53ec1928a44ed7ad0606a0247a7ce34e2c5a8e9d8906dbcbd",
    "zh:88155234e7a4d45cc91ebcb2d633fdfc2daad4e85e5b1990c864dab0432afa0e",
    "zh:b13055e38617be147e82eec8b20c579e9c202da9ead8c976a54ed08bde6b06f7",
    "zh:bc6f8f1f84afcc66c5b248ffa34580d8f7e7552628eb6ad044765513159db8e4",
    "zh:d91899fe77e7223d91d2cfed2cacde1afe8b528771402ec4d494b81457421bb1",
    "zh:ef5ca86c48a786a0cc481f4cfb1c9f2e3b8eccb640c106c6a1f253f97f5e9c55",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/null" {
  version = "3.2.2"
  hashes = [
    "h1:zT1ZbegaAYHwQa+QwIFugArWikRJI9dqohj8xb0GY88=",
    "zh:3248aae6a2198f3ec8394218d05bd5e42be59f43a3a7c0b71c66ec0df08b69e7",
    "zh:32b1aaa1c3013d33c245493f4a65465eab9436b454d250102729321a44c8ab9a",
    "zh:38eff7e470acb48f66380a73a5c7cdd76cc9b9c9ba9a7249c7991488abe22fe3",
    "zh:4c2f1faee67af104f5f9e711c4574ff4d298afaa8a420680b0cb55d7bbc65606",
    "zh:544b33b757c0b954dbb87db83a5ad921edd61f02f1dc86c6186a5ea86465b546",
    "zh:696cf785090e1e8cf1587499516b0494f47413b43cb99877ad97f5d0de3dc539",
    "zh:6e301f34757b5d265ae44467d95306d61bef5e41930be1365f5a8dcf80f59452",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:913a929070c819e59e94bb37a2a253c228f83921136ff4a7aa1a178c7cce5422",
    "zh:aa9015926cd152425dbf86d1abdbc74bfe0e1ba3d26b3db35051d7b9ca9f72ae",
    "zh:bb04798b016e1e1d49bcc76d62c53b56c88c63d6f2dfe38821afef17c416a0e1",
    "zh:c23084e1b23577de22603cff752e59128d83cfecc2e6819edadd8cf7a10af11e",
  ]
}

provider "registry.terraform.io/scaleway/scaleway" {
  version     = "2.39.0"
  constraints = "2.39.0"
  hashes = [
    "h1:gQ9uFzOu5Pbps/8zo1NCiFenRsoGrWeyznxq4YkW+/U=",
    "zh:00c14c0cfbfdbd5cbf68ffaf3b43c6fe35b54802a59eb4ab1871c63c499dbd45",
    "zh:186c505cb09b4ba2b86c78931af7debd144a8bc57c46ac19166b7733fc9526b4",
    "zh:3cbda5222c7c377d217899d488a2ae80ac7e7f73f155b96c122015f69b97f676",
    "zh:43efd2e799f2cb7121b44dc7743ed0bc7e401dbdfbd931bbe564cb2a6ccfaaec",
    "zh:4446ef7c9aba0be22dd9a3cad4bc99fef588d2813fa54b82b4f23b1085f6c043",
    "zh:45599261fb49bff0b2eea696b78d522f4abbee5c71e3d736c53becaa669e65bb",
    "zh:6998bb19a1529f25f916e8b2cbd7eaeed8996a7f02526ed36d32c5f8c0c80a13",
    "zh:841476bde7069bbbd6c61f48796afb5398d05c75e055ce9ed71f45fd6fc72414",
    "zh:890ebe73c6a9cf1f62530a7efb1af9c089ff61f3807c00af6bc1da0d2712db7f",
    "zh:a14cb620057606146a6e5b87c69c826090f979c9e75a83bdaa3882015f17f3cf",
    "zh:c69e81de944e753bc7368e3525108408d2645078f6a9296e2f62ad58a7636736",
    "zh:d437e08be6865a99c7d5f30cf973e1d85fb42b84c18f47cbac98d7fd9a40b3f2",
    "zh:db0c91f30f93fd3b32b8ca8dcbfb44f808b83c2f0f757b0bcdf11591b2831f93",
    "zh:fb4850912e767a3ed58d09df1cb3985036bd4cf6bb0614308e07b3eac0b7e2ad",
  ]
}
