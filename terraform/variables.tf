variable "project_id" {
  type        = string
  description = "The project ID that will be used as default value for project-scoped resources."
}

variable "organization_id" {
  type        = string
  description = "The organization ID that will be used as default value for organization-scoped resources."
}

variable "region" {
  type        = string
  description = "The region that will be used as default value for all resources."
  default     = "fr-par"
}

variable "zone" {
  type        = string
  description = "The zone that will be used as default value for all resources."
  default     = "fr-par-1"
}

variable "chart_namespace" {
  type        = string
  description = "The namespace to install the release into."
  default     = "killing-floor-2-dc"
}

variable "chart_create_namespace" {
  type        = bool
  description = "Create the namespace if it does not yet exist."
  default     = true
}

variable "chart_repository" {
  type        = string
  description = "Repository URL where to locate the requested chart."
  default     = "https://gitlab.com/api/v4/projects/cwxlab-fr%2Flibrary%2Fhelm%2Fkilling-floor-2-dedicated-server/packages/helm/stable"
}

variable "chart_name" {
  type        = string
  description = "Chart name to be installed."
  default     = "killing-floor-2-dedicated-server"
}

variable "chart_version" {
  type        = string
  description = "The chart version to install"
  default     = "0.2.0"
}

variable "release_name" {
  type        = string
  description = "Release name."
  default     = "kf2-dc"
}

variable "release_description" {
  type        = string
  description = "Release description."
  default     = "Killing Flood 2 dedicated server"
}

variable "release_create_timeout" {
  type        = number
  description = "Timeout for create operations."
  default     = 600
}

variable "release_admin_password" {
  type        = string
  description = "This is the master server administrator password."
}

variable "release_game_password" {
  type        = string
  description = "This sets a password players will need to enter to join the server."
  default     = ""
}

