resource "scaleway_vpc_private_network" "kf2" {}

resource "scaleway_k8s_cluster" "kf2" {
  name        = "kf2"
  description = "Killing Flood 2 K8S cluster"

  project_id = var.project_id
  region     = var.region

  private_network_id          = scaleway_vpc_private_network.kf2.id
  delete_additional_resources = true

  version = var.k8s_version
  cni     = var.k8s_cni
  tags    = var.k8s_tags
}
