terraform {
  required_providers {
    scaleway = {
      source  = "scaleway/scaleway"
      version = "2.39.0"
    }
  }
  required_version = ">= 0.13"
}
