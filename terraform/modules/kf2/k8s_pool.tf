resource "scaleway_k8s_pool" "kf2" {
  cluster_id = scaleway_k8s_cluster.kf2.id

  name   = "kf2"
  zone   = var.zone
  region = var.region

  node_type = var.k8s_node
  size      = 1
  tags      = var.k8s_tags
}

resource "null_resource" "kubeconfig" {
  depends_on = [scaleway_k8s_pool.kf2] # at least one pool here
  triggers = {
    host                   = scaleway_k8s_cluster.kf2.kubeconfig[0].host
    token                  = scaleway_k8s_cluster.kf2.kubeconfig[0].token
    cluster_ca_certificate = scaleway_k8s_cluster.kf2.kubeconfig[0].cluster_ca_certificate
  }
}
