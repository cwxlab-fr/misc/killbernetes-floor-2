variable "k8s_version" {
  type        = string
  description = "Kubernetes version"
  default     = "1.24.7"
}

variable "k8s_cni" {
  type        = string
  description = "Container Network Interface"
  default     = "cilium"
}

variable "k8s_node" {
  type        = string
  description = "Node type"
  default     = "PLAY2_NANO"
}

variable "k8s_tags" {
  type        = list(string)
  description = "Tags list"
  default     = ["k8s", "game", "Killing Floor", "Killing Floor 2", "dev"]
}

variable "project_id" {
  type        = string
  description = "The project ID that will be used as default value for project-scoped resources."
}

variable "region" {
  type        = string
  description = "The region that will be used as default value for all resources."
  default     = "fr-par"
}

variable "zone" {
  type        = string
  description = "The zone that will be used as default value for all resources."
  default     = "fr-par-1"
}
