terraform {
  required_providers {
    scaleway = {
      source  = "scaleway/scaleway"
      version = "2.39.0"
    }
  }
  required_version = ">= 0.13"
}

module "kf2" {
  source = "./modules/kf2"

  zone       = var.zone
  project_id = var.project_id
}
provider "helm" {
  kubernetes {
    host  = null_resource.kubeconfig.triggers.host
    token = null_resource.kubeconfig.triggers.token
    cluster_ca_certificate = base64decode(
      null_resource.kubeconfig.triggers.cluster_ca_certificate
    )
  }
}

resource "null_resource" "kubeconfig" {
  depends_on = [
    module.kf2.scaleway_k8s_pool,
  ]

  triggers = {
    host                   = module.kf2.host
    token                  = module.kf2.token
    cluster_ca_certificate = module.kf2.certificate
  }
}

resource "helm_release" "kf2dc" {
  depends_on = [module.kf2]

  name        = var.release_name
  description = var.release_description

  repository = var.chart_repository
  chart      = var.chart_name
  version    = var.chart_version

  namespace        = var.chart_namespace
  create_namespace = var.chart_create_namespace

  set_sensitive {
    name  = "adminPassword"
    value = var.release_admin_password
  }

  set_sensitive {
    name  = "gamePassword"
    value = var.release_game_password
  }

  set {
    name  = "storageClassName"
    value = "scw-bssd"
  }

  lint = true

  timeout = var.release_create_timeout
}

