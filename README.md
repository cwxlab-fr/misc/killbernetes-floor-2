# Killbernetes Floor 2

## Pipeline status

[![pipeline status](https://gitlab.com/cwxlab-fr/misc/killbernetes-floor-2/badges/main/pipeline.svg)](https://gitlab.com/cwxlab-fr/misc/killbernetes-floor-2/-/commits/main)

## Corverage report

[![coverage report](https://gitlab.com/cwxlab-fr/misc/killbernetes-floor-2/badges/main/coverage.svg)](https://gitlab.com/cwxlab-fr/misc/killbernetes-floor-2/-/commits/main)

## Latest release

[![Latest Release](https://gitlab.com/cwxlab-fr/misc/killbernetes-floor-2/-/badges/release.svg)](https://gitlab.com/cwxlab-fr/misc/killbernetes-floor-2/-/releases) 


## CI/CD variables

| Name | Description | Value |
|------|-------------|-------|
| SCW_ACCESS_KEY | Scaleway access key<br>[providers/scaleway/scaleway](https://registry.terraform.io/providers/scaleway/scaleway/latest/docs) | `<REDACTED>` |
| SCW_DEFAULT_ORGANIZATION_ID | The organization ID that will be used as default value for organization-scoped resources<br>[providers/scaleway/scaleway](https://registry.terraform.io/providers/scaleway/scaleway/latest/docs) | `<REDACTED>` |
| SCW_DEFAULT_PROJECT_ID | The project ID that will be used as default value for project-scoped resources<br>[providers/scaleway/scaleway](https://registry.terraform.io/providers/scaleway/scaleway/latest/docs) | `<REDACTED>` |
| SCW_SECRET_KEY | Scaleway secret key<br>[providers/scaleway/scaleway](https://registry.terraform.io/providers/scaleway/scaleway/latest/docs) | `<REDACTED>` |
| TF_HTTP_ADDRESS | The address of the REST endpoint<br>[backends/http#address](https://developer.hashicorp.com/terraform/language/settings/backends/http#address) | `${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${CI_PROJECT_PATH_SLUG}-${CI_ENVIRONMENT_SLUG}` |
| TF_HTTP_LOCK_ADDRESS | The address of the lock REST endpoint<br>[backends/http#lock_address](https://developer.hashicorp.com/terraform/language/settings/backends/http#lock_address) | `${TF_HTTP_ADDRESS}/lock` |
| TF_HTTP_LOCK_METHOD | The HTTP method to use when locking<br>[backends/http#lock_method](https://developer.hashicorp.com/terraform/language/settings/backends/http#lock_method) | `POST` |
| TF_HTTP_PASSWORD | The password for HTTP basic authentication<br>[backends/http#password](https://developer.hashicorp.com/terraform/language/settings/backends/http#password) | `${CI_JOB_TOKEN}` |
| TF_HTTP_RETRY_WAIT_MIN |The minimum time in seconds to wait between HTTP request attempts<br>[backends/http#retry_wait_min](https://developer.hashicorp.com/terraform/language/settings/backends/http#retry_wait_min) | `5` |
| TF_HTTP_UNLOCK_ADDRESS | The address of the unlock REST endpoint<br>[backends/http#unlock_address](https://developer.hashicorp.com/terraform/language/settings/backends/http#unlock_address)| `${TF_HTTP_ADDRESS}/lock` |
| TF_HTTP_UNLOCK_METHOD | The HTTP method to use when unlocking<br>[backends/http#unlock_method](https://developer.hashicorp.com/terraform/language/settings/backends/http#unlock_method) | `DELETE` |
| TF_HTTP_USERNAME | The username for HTTP basic authentication<br>[backends/http#username](https://developer.hashicorp.com/terraform/language/settings/backends/http#username) | `gitlab-ci-token` |

